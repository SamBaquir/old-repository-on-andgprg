using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{
    public Transform nozzle1;
    public Transform nozzle2;
    public Transform nozzle3;
    public Transform nozzle4;


    public GameObject bulletPrefab1;
    public GameObject bulletPrefab2;
    public GameObject bulletPrefab3;
    public GameObject bulletPrefab4;

    bool firstNozzleActive = false;
    bool secondNozzleActive = false;
    bool thirdNozzleActive = false;
    bool fourthNozzleActive = false;

    void Awake()
    {
        firstNozzleActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && firstNozzleActive)
        {
            Destroy(Instantiate(bulletPrefab1, nozzle1.transform.position, transform.rotation), 5.0f);
        }
        else if (Input.GetKeyDown(KeyCode.Space) && secondNozzleActive)
        {
            Destroy(Instantiate(bulletPrefab2, nozzle2.transform.position, transform.rotation), 5.0f);
        }
        else if (Input.GetKeyDown(KeyCode.Space) && thirdNozzleActive)
        {
            Destroy(Instantiate(bulletPrefab3, nozzle3.transform.position, transform.rotation), 5.0f);
        }
        else if (Input.GetKeyDown(KeyCode.Space) && fourthNozzleActive)
        {
            Destroy(Instantiate(bulletPrefab4, nozzle4.transform.position, transform.rotation), 5.0f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            firstNozzleActive = true;
            secondNozzleActive = false;
            thirdNozzleActive = false;
            fourthNozzleActive = false;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            firstNozzleActive = false;
            secondNozzleActive = true;
            thirdNozzleActive = false;
            fourthNozzleActive = false;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            firstNozzleActive = false;
            secondNozzleActive = false;
            thirdNozzleActive = true;
            fourthNozzleActive = false;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            firstNozzleActive = false;
            secondNozzleActive = false;
            thirdNozzleActive = false;
            fourthNozzleActive = true;
        }

    }
}
